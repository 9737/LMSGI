<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<html>
	<head>
	<title>Lecturas Recomendadas para final de curso</title>
	<link href="libreria.css" rel="stylesheet" type="text/css"/>
	</head><body><h1>Lecturas Recomendadas para final de curso</h1>
	<table border="0">
	<tr>
	<th>ISBN</th>
	<th>Título</th>
	<th>Autor</th>
	<th>Precio</th>
	</tr>
	<xsl:for-each select="
		libreria / libro ">
		<tr>
			<td>
				<xsl:value-of select="isbn" />
			</td>
			<td>
				<xsl:value-of select="titulo" />
			</td>
			<td>
				<xsl:value-of select="autor" />
			</td>
			<td>
				<xsl:value-of select="precio" />
			</td>
		</tr>
	</xsl:for-each>
	</table>
	</body>
	</html>
	</xsl:template>
</xsl:stylesheet>